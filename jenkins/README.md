This setup makes use of the ec2.py script for dynamic inventory on AWS.
This allows us to use the "role" tag for ec2 instances so that we never have to
create a hosts file.

This is designed to have very little hard-coded into the terraform files.
All that needs to be done is to prefix the makefile with AWS environment
variables.

The examples provided use the "env" prefix which is specific to the fish shell.
If you are using a posix compliant shell, just delete the "env " from the
lead line.

After running a "make jenkins", you will have a fully functional ubuntu ec2 host
that you can log into with admin:admin. Nginx will reverse proxy to the jenkins
server, so just the ip address is necessary.

The only things that will need to be changed are the ami and the instance type.
Set these to match the region and scale that you input.
A relevant list can be fonud here:
https://cloud-images.ubuntu.com/locator/ec2/


Examples:

To do a complete install of jenkins:

env AWS_ACCESS_KEY_ID=yourAccessKeyGoesHere \
AWS_SECRET_ACCESS_KEY=yourSecretKeyGoesHere \
AWS_DEFAULT_REGION=yourDesiredRegionGoesHere \
make jenkins


To do a terraform apply:

env AWS_ACCESS_KEY_ID=yourAccessKeyGoesHere \
AWS_SECRET_ACCESS_KEY=yourSecretKeyGoesHere \
AWS_DEFAULT_REGION=yourDesiredRegionGoesHere \
make apply


To do a terraform destroy:

env AWS_ACCESS_KEY_ID=yourAccessKeyGoesHere \
AWS_SECRET_ACCESS_KEY=yourSecretKeyGoesHere \
AWS_DEFAULT_REGION=yourDesiredRegionGoesHere \
make destroy


To clean up:

make clean