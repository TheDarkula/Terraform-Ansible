provider "aws" { }

resource "aws_key_pair" "jenkins" {
  key_name = "jenkins"
  public_key = "${file("keys/jenkins.pub")}"
}

resource "aws_security_group" "ssh_jenkins" {
    name = "ssh"
    description = "allow ssh access"

    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_security_group" "nginx_jenkins" {
    name = "nginx"
    description = "allow web traffic"

    ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
      from_port = 443
      to_port = 443
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_instance" "jenkins" {
    ami = "ami-835b4efa"
    instance_type = "t2.micro"
    key_name = "${aws_key_pair.jenkins.key_name}"
    security_groups = ["${aws_security_group.ssh_jenkins.name}", "${aws_security_group.nginx_jenkins.name}"]

    tags {
        Name = "jenkins"
        role = "jenkins"
    }
}

resource "aws_eip" "jenkins" {
    instance = "${aws_instance.jenkins.id}"
    vpc = true
}